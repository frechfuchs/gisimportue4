# GISimportUE4 POC

- Requires Unreal Engine 4.19
- Clone & have fun!

Used Plugin: https://github.com/ue4plugins/StreetMap

Screenshot:
![](https://bitbucket.org/frechfuchs/gisimportue4/raw/dce423f8275200e7db89c23bd86035cd4013809a/Screenshot.png)